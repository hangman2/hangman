import sys
import pygame
import random
import tkinter as tk
from PIL import ImageTk,Image
pygame.init()
win = pygame.display.set_caption('Hangman')
winHeight = 620
winWidth =1000
win=pygame.display.set_mode((winWidth,winHeight))
BLACK = (0,0, 0)
WHITE = (255,255,255)
RED = (255,0, 0)
GREEN = (0,255,0)
BLUE = (0,0,255)
LIGHT_BLUE = (102,255,255)

btn_font = pygame.font.SysFont("italic", 30)
guess_font = pygame.font.SysFont("monospace", 24)
lost_font = pygame.font.SysFont('arial', 45)
word = ''
buttons = []
guessed = []
hangmanPics = [pygame.image.load('hang01.png'), pygame.image.load('hang02.png'), pygame.image.load('hang03.png'), pygame.image.load('hang04.png'), pygame.image.load('hang05.png'), pygame.image.load('hang06.png'), pygame.image.load('hang07.png'),pygame.image.load('hang08.png'),pygame.image.load('hang09.png'),pygame.image.load('hang10.png')]
                                                                                                    
limbs = 0


def redraw_game_window():
    global guessed
    global hangmanPics
    global limbs
    win.fill(WHITE)
    # Buttons
    for i in range(len(buttons)):
        if buttons[i][4]:
            pygame.draw.circle(win,BLUE,(buttons[i][1],buttons[i][2]),buttons[i][3])
            pygame.draw.circle(win, buttons[i][0], (buttons[i][1], buttons[i][2]), buttons[i][3] - 2
                               )
            label = btn_font.render(chr(buttons[i][5]), 1, BLACK)
            win.blit(label, (buttons[i][1] - (label.get_width() / 2), buttons[i][2] - (label.get_height() / 2)))

    spaced = spacedOut(word, guessed)
    label1 = guess_font.render(spaced, 1, BLACK)
    rect = label1.get_rect()
    length = rect[2]
    
    win.blit(label1,(winWidth/2 - length/2, 400))

    pic = hangmanPics[limbs]
    win.blit(pic, (winWidth/2 - pic.get_width()/2 + 20, 150))
    pygame.display.update()



def randomWord():
    root=tk.Tk()
    can = tk.Canvas(root)
    can.grid()
    bg = tk.PhotoImage(file = "nature.png")
    l = tk.Label(root,image = bg)
    l.place(x = 0,y = 0)
    #can.create_image(1,1,anchor = tk.NW,image = my_im)
    #can.configure(bg = 'green')
    root.title('player2')
    # setting the windows size
    root.geometry("1000x620")
    secret_word=tk.StringVar()
    #global secretWord
    def submit():
        #global secretWord
        submit.secretWord = secret_word.get()	
        #print("The secret_word is : " + secretWord)
        secret_word.set("")
        root.destroy()
        
    word_label = tk.Label(root, text = 'Secret Word: ', font = ('italic',20,'bold'),bg = 'light blue')
    root.wm_attributes('-transparentcolor','grey') 

    word_entry=tk.Entry(root, textvariable = secret_word, font = ('calibre',20,'normal'), show = '*',bg  = 'light blue')

    sub_btn=tk.Button(root,text = 'Submit', command = submit,font = (15))

    word_label.grid(row=3,column=0)
    word_entry.grid(row=3,column=1)
    sub_btn.grid(row=4,column=1)
    root.mainloop()
    #submit()
    return submit.secretWord



def hang(guess):
    global word
    if guess.lower() not in word.lower():
        return True
    else:
        return False


def spacedOut(word, guessed=[]):
    spacedWord = ''
    guessedLetters = guessed
    for x in range(len(word)):
        if word[x] != ' ':
            spacedWord += '_ '
            for i in range(len(guessedLetters)):
                if word[x].upper() == guessedLetters[i]:
                    spacedWord = spacedWord[:-2]
                    spacedWord += word[x].upper() + ' '
        elif word[x] == ' ':
            spacedWord += ' '
    return spacedWord
            

def buttonHit(x, y):
    for i in range(len(buttons)):
        if x < buttons[i][1] + 20 and x > buttons[i][1] - 20:
            if y < buttons[i][2] + 50 and y > buttons[i][2] - 20:
                return buttons[i][5]
    return None


clicked = False
counter = 0

class button():
	font1 = pygame.font.SysFont('Constantia', 30)	
	#colours for button and text
	button_col = (230, 220, 170)
	hover_col = (75, 225, 255)
	click_col = (50, 150, 255)
	text_col = BLACK
	width = 180
	height = 70

	def __init__(self, x, y, text):
		self.x = x
		self.y = y
		self.text = text

	def draw_button(self):

		global clicked
		action = False

		#get mouse position
		pos = pygame.mouse.get_pos()

		#create pygame Rect object for the button
		button_rect = pygame.Rect(self.x, self.y, self.width, self.height)
		
		#check mouseover and clicked conditions
		if button_rect.collidepoint(pos):
			if pygame.mouse.get_pressed()[0] == 1:
				clicked = True
				pygame.draw.rect(win, self.click_col, button_rect)
			elif pygame.mouse.get_pressed()[0] == 0 and clicked == True:
				clicked = False
				action = True
			else:
				pygame.draw.rect(win, self.hover_col, button_rect)
		else:
			pygame.draw.rect(win, self.button_col, button_rect)
		
		#add shading to button
		pygame.draw.line(win, WHITE, (self.x, self.y), (self.x + self.width, self.y), 2)
		pygame.draw.line(win, WHITE, (self.x, self.y), (self.x, self.y + self.height), 2)
		pygame.draw.line(win, BLACK, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), 2)
		pygame.draw.line(win, BLACK, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), 2)

		#add text to button
		text_img = pygame.font.SysFont('Constantia',30).render(self.text, True, self.text_col)	
		text_len = text_img.get_width()
		win.blit(text_img, (self.x + int(self.width / 2) - int(text_len / 2), self.y + 25))
		return action

def end(winner=False):
    global limbs
    lostTxt = 'You Lost, press any key to play again...'
    winTxt = 'WINNER!, press any key to play again...'
    redraw_game_window()
    pygame.time.delay(1000)
    win.fill(WHITE)
    i = pygame.image.load('hang11.png')
    i1 = pygame.image.load('hang10.png')
    if winner == True:
        win.blit(i,(0,100))
        label = lost_font.render(winTxt, 1, BLACK)
        wordTxt = lost_font.render(word.upper(), 1, BLACK)
        wordWas = lost_font.render('The word was: ', 1, BLACK)
        win.blit(wordTxt, (winWidth/2 - wordTxt.get_width()/2, 295))
        win.blit(wordWas, (winWidth/2 - wordWas.get_width()/2, 245))
        win.blit(label, (winWidth / 2 - label.get_width() / 2, 140))
        quit = button(700,500,'Quit :(')
        run = True
        while run:
            if quit.draw_button():
                    sys.exit()
	    #print('Quit')   
            for event in pygame.event.get():
	            if event.type == pygame.QUIT:
		            run = False		
            pygame.display.update()
    else:
        label = lost_font.render(lostTxt, 1, BLACK)
        win.blit(i1,(0,100))
        wordTxt = lost_font.render(word.upper(), 1, BLACK)
        wordWas = lost_font.render('The word was: ', 1, BLACK)
        win.blit(wordTxt, (winWidth/2 - wordTxt.get_width()/2, 295))
        win.blit(wordWas, (winWidth/2 - wordWas.get_width()/2, 245))
        win.blit(label, (winWidth / 2 - label.get_width() / 2, 140))
        quit = button(700,500,'Quit :(')
        run = True
        while run:
            if quit.draw_button():
                    sys.exit()
	    #print('Quit')   
            for event in pygame.event.get():
	            if event.type == pygame.QUIT:
		            run = False		
            pygame.display.update()


    
    pygame.display.update()
    again = True
    while again:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                again = False
    reset()


def reset():
    global limbs
    global guessed
    global buttons
    global word
    for i in range(len(buttons)):
        buttons[i][4] = True

    limbs = 0
    guessed = []
    word = randomWord()

#MAINLINE


# Setup buttons
increase = round(winWidth / 13)
for i in range(26):
    if i < 13:
        y = 40
        x = 25 + (increase * i)
    else:
        x = 25 + (increase * (i - 13))
        y = 85
    buttons.append([LIGHT_BLUE, x, y, 20, True, 65 + i])

word = randomWord()
inPlay = True

while inPlay:
    redraw_game_window()
    pygame.time.delay(10)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            inPlay = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                inPlay = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            clickPos = pygame.mouse.get_pos()
            letter = buttonHit(clickPos[0], clickPos[1])
            if letter != None:
                guessed.append(chr(letter))
                buttons[letter - 65][4] = False
                if hang(chr(letter)):
                    if limbs != 8:
                        limbs += 1
                    else:
                        end()
                else:
                    print(spacedOut(word, guessed))
                    if spacedOut(word, guessed).count('_') == 0:
                        end(True)

pygame.quit()


